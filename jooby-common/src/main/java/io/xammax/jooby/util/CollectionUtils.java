package io.xammax.jooby.util;

import java.util.Collection;
import java.util.Map;

public abstract class CollectionUtils {

  /**
   * Null safe empty check.
   *
   * @param collection the Collection to check
   * @return true whether the given collection is empty or {@literal null}
   */
  public static boolean isEmpty(Collection<?> collection) {
    return (collection == null || collection.isEmpty());
  }

  /**
   * Null safe empty check.
   *
   * @param map the map to check
   * @return true whether the given map is empty or {@literal null}
   */
  public static boolean isEmpty(Map<?, ?> map) {
    return (map == null || map.isEmpty());
  }

  /**
   * Null safe not empty check.
   *
   * @param map the map to check
   * @return true whether the given map is not empty and not {@literal null}
   */
  public static boolean isNotEmpty(Map<?, ?> map) {
    return map != null && !map.isEmpty();
  }

  /**
   * Null safe not empty check.
   *
   * @param collection the collection to check
   * @return true whether the given collection is not empty and not {@literal null}
   */
  public static boolean isNotEmpty(Collection<?> collection) {
    return collection != null && !collection.isEmpty();
  }

}
