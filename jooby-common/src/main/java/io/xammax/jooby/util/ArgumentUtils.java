package io.xammax.jooby.util;

public abstract class ArgumentUtils {

  /**
   * Assert that an object is not {@literal null}.
   *
   * <pre class="code">
   *     Assert.notNull(clazz, "The class must not be null");
   * </pre>
   *
   * @param object the object to check
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if the object is {@literal null}
   */
  public static void notNull(Object object, String message) {
    if (object == null) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Assert a boolean expression, throwing an {@link IllegalArgumentException} if the expression
   * evaluates to {@literal false}.
   *
   * <pre class="code">
   *     Assert.isTrue(i &gt; 0, "The value must be greater than zero");
   * </pre>
   *
   * @param expression a boolean expression
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if {@literal expression} is {@literal false}
   */
  public static void isTrue(boolean expression, String message) {
    if (!expression) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Adds a check that the given number (object) is not {@literal null}.
   *
   * @param name the argument name
   * @param value the argument value
   * @param <T> The generic type of argument value
   * @throws NullPointerException if the argument is {@literal null}
   */
  public static <T> void requireNonNull(String name, T value) {
    if (value == null) {
      throw new NullPointerException("Argument [" + name + "] cannot be null");
    }
  }

}
