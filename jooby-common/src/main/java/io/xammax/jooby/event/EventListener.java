package io.xammax.jooby.event;

public interface EventListener<E extends Event> extends java.util.EventListener {

    /**
     * The {@link EventListener} interface is the primary method for handling events, registers
     * their listener on an {@link EventManager} using the {@code EventManager#AddEventListener}
     * method. {@link EventListener} must be removed from {@link EventManager} after completed
     * using the listener.
     *
     * @param event the {@link Event} contains contextual information about the event. It also
     *              contains the stopPropagation and preventDefault methods which are used in
     *              determining the event's flow and default action.
     */
    void handleEvent(E event);

}