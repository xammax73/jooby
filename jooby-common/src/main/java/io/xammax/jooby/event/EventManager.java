package io.xammax.jooby.event;

import javax.annotation.Nonnull;
import java.io.IOException;

public interface EventManager<E extends Event> {

    /**
     * Publishes event for a him listen on {@link EventListener}s method.
     *
     * @param event specifies event that needs to listen on {@link EventListener}
     * @throws NullPointerException if the event is {@code null}
     * @throws IllegalArgumentException if the event is not inherited from {@link Event}
     */
    void publish(@Nonnull E event) throws IOException, InterruptedException;

    /**
     * Registers a listener to receive events.
     *
     * @param eventType specifies event type for which the {@link EventListener} is registering
     * @param listener the {@link EventListener} parameter takes an interface which contains
     *                 the methods to be called when the event occurs
     * @param useCapture if {@literal true}, parameter indicates that need to initiate capture.
     *                   After initiating capture, all events of the specified type will be
     *                   dispatched to the registered {@link EventListener} before being
     *                   dispatched to any {@link EventManager} beneath them in the tree.
     *                   Events which are bubbling upward through the tree will not trigger an
     *                   {@link EventListener} designated to use capture.
     * @throws NullPointerException if the listener is {@code null}
     * @throws IllegalArgumentException if the parameter is not found to be an actual listener
     */
    void register(String eventType, @Nonnull EventListener<E> listener, boolean useCapture);

    /**
     * Un-registers a listener so that it will no longer receive events. If the given listener
     * is not registered nothing will happen.
     *
     * @param eventType specifies the event type of the {@link EventListener} being removed
     * @param listener parameter that indicates which the {@link EventListener} to be removed
     * @param useCapture specifies whether the EventListener being removed was registered as a
     *                   capturing listener or not. If a listener was registered twice, one with
     *                   capture and one without, each must be removed separately. Removal of a
     *                   capturing listener does not affect a non-capturing version of the same
     *                   listener, and vice versa
     * @throws NullPointerException if the listener is {@code null}
     * @throws IllegalArgumentException if the parameter is not found to be an actual listener
     */
    void unregister(String eventType, @Nonnull EventListener<E> listener, boolean useCapture);

}
