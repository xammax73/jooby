package io.xammax.jooby.hibernate.injector;

import io.jooby.Extension;
import io.jooby.Jooby;

import javax.annotation.Nonnull;

import static io.xammax.jooby.JoobyInstance.app;

public class HibernateExtension implements Extension {

  public void install(@Nonnull Jooby application) {
    /* application */
    app = application;
  }

}
