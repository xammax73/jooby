package io.xammax.jooby.hibernate;

import io.xammax.jooby.internal.hibernate.*;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.*;

public class HibernateSearchProcessor extends SearchProcessor {

  private final static Logger logger = LoggerFactory.getLogger(HibernateSearchProcessor.class);
  private final static Map<SessionFactory, HibernateSearchProcessor> map = new HashMap<>();

  public static @Nonnull HibernateSearchProcessor getInstanceForSessionFactory(SessionFactory sessionFactory) {
    HibernateSearchProcessor instance = map.get(sessionFactory);
    if (instance == null) {
      instance = new HibernateSearchProcessor(HibernateMetadataUtils.getInstanceForSessionFactory(sessionFactory));
      map.put(sessionFactory, instance);
    }
    return instance;
  }

  private HibernateSearchProcessor(HibernateMetadataUtils mdu) {
    super(QLTYPE_HQL, mdu);
  }

  // Public methods

  /**
   * Search for objects based on the search parameters in the {@link ImmutableSearch} object.
   */
  public <RT> List<RT> search(Session session, ImmutableSearch search) {
    if (search == null)
      return null;

    return search(session, search.getSearchClass(), search);
  }

  /**
   * Search for objects based on the search parameters in the specified {@link ImmutableSearch}
   * object. Uses the specified searchClass, ignoring the searchClass specified on the search
   * itself.
   */
  @SuppressWarnings("unchecked")
  public <RT> List<RT> search(Session session, Class<?> searchClass, ImmutableSearch search) {
    if (searchClass == null || search == null) {
      return null;
    }

    List<Object> paramList = new ArrayList<>();
    String hql = generateQL(searchClass, search, paramList);
    Query<RT> query = session.createQuery(hql);
    addParams(query, paramList);
    addPaging(query, search);
    addResultMode(query, search);
    return query.getResultList();
  }

  /**
   * Returns the total number of results that would be returned using the given type of
   * {@link ImmutableSearch} if there were no paging or {@literal maxResult} limits.
   */
  public long count(Session session, ImmutableSearch search) {
    if (search == null)
      return 0;
    return count(session, search.getSearchClass(), search);
  }

  /**
   * Returns the total number of results that would be returned using the given type of
   * {@link ImmutableSearch} if there were no paging or {@literal maxResult} limits. Uses
   * the specified {@literal searchClass}, ignoring the {@literal searchClass} specified
   * on the search itself.
   */
  public long count(Session session, Class<?> searchClass, ImmutableSearch search) {
    if (searchClass == null || search == null)
      return 0;

    List<Object> paramList = new ArrayList<>();
    String hql = generateRowCountQL(searchClass, search, paramList);
    if (hql == null) { // special case where the query uses column operators
      return 1;
    }
    Query<?> query = session.createQuery(hql);
    addParams(query, paramList);

    return ((Number) query.getSingleResult()).longValue();
  }

  /**
   * Returns a {@link SearchResult} object that includes the list of results like
   * {@code search()} and the total length like {@literal searchLength}.
   */
  public <RT> SearchResult<RT> searchAndCount(Session session, ImmutableSearch search) {
    if (search == null)
      return null;
    return searchAndCount(session, search.getSearchClass(), search);
  }

  /**
   * Returns a {@link SearchResult} object that includes the list of results like
   * {@code search()} and the total length like {@literal searchLength}). Uses the
   * specified {@literal searchClass}, ignoring the {@literal searchClass} specified
   * on the search itself.
   */
  public <RT> SearchResult<RT> searchAndCount(Session session, Class<?> searchClass, ImmutableSearch search) {
    if (searchClass == null || search == null)
      return null;

    SearchResult<RT> result = new SearchResult<>();
    result.setResult(search(session, searchClass, search));

    if (search.getMaxResults() > 0) {
      result.setTotalCount(count(session, searchClass, search));
    } else {
      result.setTotalCount(result.getResult().size() + SearchUtils.calcFirstResult(search));
    }

    return result;
  }

  /**
   * Search for a single result using the given parameters.
   */
  public <RT> RT searchUnique(Session session, ImmutableSearch search) throws NonUniqueResultException {
    if (search == null)
      return null;
    return searchUnique(session, search.getSearchClass(), search);
  }

  /**
   * Search for a single result using the given parameters. Uses the specified parameter
   * {@literal searchClass}, ignoring the {@literal searchClass} specified on the search itself.
   */
  @SuppressWarnings("unchecked")
  public <RT> RT searchUnique(Session session, Class<?> searchClass, ImmutableSearch search) throws NonUniqueResultException {
    if (searchClass == null || search == null)
      return null;

    List<Object> paramList = new ArrayList<>();
    String hql = generateQL(searchClass, search, paramList);
    Query<RT> query = session.createQuery(hql);
    addParams(query, paramList);
    addPaging(query, search);
    addResultMode(query, search);

    return query.getSingleResult();
  }

  // Search helpers

  private void addParams(Query<?> query, @Nonnull List<Object> params) {
    StringBuilder debug = null;

    int i = 1;
    for (Object o : params) {
      if (logger.isDebugEnabled()) {
        if (debug == null)
          debug = new StringBuilder();
        else
          debug.append("\n\t");
        debug.append("p");
        debug.append(i);
        debug.append(": ");
        debug.append(InternalUtils.paramDisplayString(o));
      }
      if (o instanceof Collection) {
        query.setParameter("p" + (i++), o);
      } else if (o instanceof Object[]) {
        query.setParameter("p" + (i++), o);
      } else {
        query.setParameter("p" + (i++), o);
      }
    }
    if (debug != null && debug.length() != 0) {
      logger.debug(debug.toString());
    }
  }

  private void addPaging(Query<?> query, ImmutableSearch search) {
    int firstResult = SearchUtils.calcFirstResult(search);
    if (firstResult > 0) {
      query.setFirstResult(firstResult);
    }
    if (search.getMaxResults() > 0) {
      query.setMaxResults(search.getMaxResults());
    }
  }

  private void addResultMode(Query<?> query, @Nonnull ImmutableSearch search) {
    Result resultMode = search.getResultMode();
    if (resultMode == Result.RESULT_AUTO) {
      int count = 0;
      for (Field field : search.getFields()) {
        if (field.getKey() != null && !field.getKey().equals("")) {
          resultMode = Result.RESULT_MAP;
          break;
        }
        count++;
      }
      if (resultMode == Result.RESULT_AUTO) {
        if (count > 1)
          resultMode = Result.RESULT_ARRAY;
        else
          resultMode = Result.RESULT_SINGLE;
      }
    }

    switch (resultMode) {
      case RESULT_ARRAY:
        query.setParameter(String.valueOf(Result.RESULT_ARRAY), ARRAY_RESULT_TRANSFORMER);
        break;
      case RESULT_LIST:
        query.setParameter(String.valueOf(Result.RESULT_LIST), Transformers.TO_LIST);
        break;
      case RESULT_MAP:
        List<String> keyList = new ArrayList<>();
        for (Field field : search.getFields()) {
          if (field.getKey() != null && !field.getKey().equals("")) {
            keyList.add(field.getKey());
          } else {
            keyList.add(field.getProperty());
          }
        }
        query.setParameter(String.valueOf(Result.RESULT_MAP), new MapResultTransformer(keyList.toArray(new String[0])));
        break;
      default: // ISearch.RESULT_SINGLE
        break;
    }
  }

  private static final ResultTransformer ARRAY_RESULT_TRANSFORMER = new ResultTransformer() {
    private static final long serialVersionUID = 1L;

    public List transformList(List collection) {
      return collection;
    }

    public Object transformTuple(Object[] tuple, String[] aliases) {
      return tuple;
    }
  };

  private static class MapResultTransformer implements ResultTransformer {
    private static final long serialVersionUID = 1L;

    private String[] keys;

    public MapResultTransformer(String[] keys) {
      this.keys = keys;
    }

    public List transformList(List collection) {
      return collection;
    }

    public Object transformTuple(Object[] tuple, String[] aliases) {
      Map<String, Object> map = new HashMap<>();
      for (int i = 0; i < keys.length; i++) {
        String key = keys[i];
        if (key != null) {
          map.put(key, tuple[i]);
        }
      }

      return map;
    }
  }

}
