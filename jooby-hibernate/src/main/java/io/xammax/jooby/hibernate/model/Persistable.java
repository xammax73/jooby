package io.xammax.jooby.hibernate.model;

import java.io.Serializable;

/**
 * Defines a class as persistable.
 *
 * @param <ID> the type of the entity identifier
 */
public interface Persistable<ID extends Serializable> extends Identifiable<ID>, Serializable {

  /**
   * Returns boolean value in accordance with {@link Persistable} is new or persisted already.
   *
   * @return {@literal true} if the object is new, otherwise {@literal false}
   */
  boolean isNew();

}