package io.xammax.jooby.hibernate.repository;

import io.xammax.jooby.hibernate.HibernateSearchProcessor;
import io.xammax.jooby.hibernate.model.Persistable;
import io.xammax.jooby.internal.hibernate.ImmutableSearch;
import io.xammax.jooby.internal.hibernate.Search;
import io.xammax.jooby.internal.hibernate.SearchResult;
import org.hibernate.NonUniqueResultException;

import java.io.Serializable;
import java.util.List;

public class SearchRepositoryImpl<T extends Persistable<ID>, ID extends Serializable>
    extends CrudRepositoryImpl<T, ID> implements SearchRepository<T, ID> {

  private final HibernateSearchProcessor searchProcessor;

  public SearchRepositoryImpl() {
    super();
    searchProcessor = HibernateSearchProcessor.getInstanceForSessionFactory(sessionFactory);
  }

  public HibernateSearchProcessor getSearchProcessor() {
    return searchProcessor;
  }

  // Public methods

  public long count() {
    return _count(persistentClass);
  }

  public long count(ImmutableSearch search) {
    if (search == null)
      search = new Search();
    return _count(persistentClass, search);
  }

  /* *
  public long countAll() {
    return _countAll(persistentClass);
  }
   */

  @SuppressWarnings("unchecked")
  public <RT> List<RT> search(ImmutableSearch search) {
    if (search == null)
      return (List<RT>) findAll();
    return _search(persistentClass, search);
  }

  @SuppressWarnings("unchecked")
  public <RT> SearchResult<RT> searchAndCount(ImmutableSearch search) {
    if (search == null) {
      SearchResult<RT> result = new SearchResult<>();
      result.setResult((List<RT>) findAll());
      result.setTotalCount(result.getResult().size());
      return result;
    }
    return _searchAndCount(persistentClass, search);
  }

  @SuppressWarnings("unchecked")
  public <RT> RT searchUnique(ImmutableSearch search) {
    return (RT) _searchUnique(persistentClass, search);
  }

  // Protected methods

  /**
   * Retrieves the total number of results that would be returned using the given
   * {@link ImmutableSearch} if there were no paging or maxResult limits.
   */
  protected long _count(ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (search.getSearchClass() == null)
      throw new NullPointerException("Search class is null.");

    return getSearchProcessor().count(getSession(), search);
  }

  /**
   * Same as {@code _count(ImmutableSearch)} except that it uses the specified search class
   * instead of getting it from the search object. Also, if the search object has a different
   * search class than what is specified, an exception is thrown.
   */
  protected long _count(Class<?> searchClass, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (searchClass == null)
      throw new NullPointerException("Search class is null.");
    if (search.getSearchClass() != null && !search.getSearchClass().equals(searchClass))
      throw new IllegalArgumentException("Search class does not match expected type: " + searchClass.getName());

    return getSearchProcessor().count(getSession(), searchClass, search);
  }

  /**
   * Retrieves the total number of instances of this class.
   */
  protected long _count(Class<?> type) {
    List<?> counts = getSession()
        .createQuery("select count(_it_) from " + getMetadataUtils().get(type).getEntityName() + " _it_")
        .getResultList();
    int sum = 0;
    for (Object count : counts) {
      sum += ((long) count);
    }
    return sum;
  }

    /* *
    protected long _count(Class<?> type) {
      type = getMetadataUtils().getUnproxiedClass(type);
      return getSession()
          .createQuery("select count(*) from " + type.getSimpleName(), Long.class)
          .uniqueResult();
    }
    */

    /* *
    protected long _countAll(Class<?> type) {
      type = getMetadataUtils().getUnproxiedClass(type);
      CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
      CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
      criteriaQuery.select(criteriaBuilder.count(criteriaQuery.from(type)));
      return getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }
     */

  /**
   * Search for objects based on the search parameters in the specified {@link ImmutableSearch}
   * object.
   */
  protected List<?> _search(ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (search.getSearchClass() == null)
      throw new NullPointerException("Search class is null.");

    return getSearchProcessor().search(getSession(), search);
  }

  /**
   * Same as {@code _search(ImmutableSearch)} except that it uses the specified search class
   * instead of getting it from the search object. Also, if the search object has a different
   * search class than what is specified, an exception is thrown.
   */
  protected <RT> List<RT> _search(Class<?> searchClass, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (searchClass == null)
      throw new NullPointerException("Search class is null.");
    if (search.getSearchClass() != null && !search.getSearchClass().equals(searchClass))
      throw new IllegalArgumentException("Search class does not match expected type: " + searchClass.getName());

    return getSearchProcessor().search(getSession(), searchClass, search);
  }

  /**
   * Returns a {@link SearchResult} object that includes the list of results like
   * {@code search()} and the total length like {@literal searchLength}.
   */
  protected <RT> SearchResult<RT> _searchAndCount(ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (search.getSearchClass() == null)
      throw new NullPointerException("Search class is null.");

    return getSearchProcessor().searchAndCount(getSession(), search);
  }

  /**
   * Same as {@code _searchAndCount(ImmutableSearch)} except that it uses the specified search
   * class instead of getting it from the search object. Also, if the search object has a
   * different search class than what is specified an exception is thrown.
   */
  protected <RT> SearchResult<RT> _searchAndCount(Class<?> searchClass, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (searchClass == null)
      throw new NullPointerException("Search class is null.");
    if (search.getSearchClass() != null && !search.getSearchClass().equals(searchClass))
      throw new IllegalArgumentException("Search class does not match expected type: " + searchClass.getName());

    return getSearchProcessor().searchAndCount(getSession(), searchClass, search);
  }

  /**
   * Search for a single result using the given parameters.
   */
  protected Object _searchUnique(ImmutableSearch search) throws NonUniqueResultException {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (search.getSearchClass() == null)
      throw new NullPointerException("Search class is null.");

    return getSearchProcessor().searchUnique(getSession(), search);
  }

  /**
   * Same as {@code _searchUnique(ImmutableSearch)} except that it uses the specified search
   * class instead of getting it from the search object. Also, if the search object has a
   * different search class than what is specified, an exception is thrown.
   */
  protected Object _searchUnique(Class<?> searchClass, ImmutableSearch search) {
    if (search == null)
      throw new NullPointerException("Search is null.");
    if (searchClass == null)
      throw new NullPointerException("Search class is null.");
    if (search.getSearchClass() != null && !search.getSearchClass().equals(searchClass))
      throw new IllegalArgumentException("Search class does not match expected type: " + searchClass.getName());

    return getSearchProcessor().searchUnique(getSession(), searchClass, search);
  }

}
