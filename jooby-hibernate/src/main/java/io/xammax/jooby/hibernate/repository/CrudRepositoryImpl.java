package io.xammax.jooby.hibernate.repository;

import io.xammax.jooby.hibernate.HibernateMetadataUtils;
import io.xammax.jooby.hibernate.model.Persistable;
import io.xammax.jooby.util.ReflectionUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static io.xammax.jooby.JoobyInstance.app;

@SuppressWarnings("unchecked")
public class CrudRepositoryImpl<T extends Persistable<ID>, ID extends Serializable> implements CrudRepository<T, ID> {

  protected final static Logger logger = LoggerFactory.getLogger(CrudRepositoryImpl.class);
  protected EntityManagerFactory entityManagerFactory;
  protected SessionFactory sessionFactory;
  protected Session session;
  protected final HibernateMetadataUtils metadataUtils;
  protected Class<T> persistentClass;

  public CrudRepositoryImpl() {
    this.entityManagerFactory = app.require(EntityManagerFactory.class);
    this.sessionFactory = app.require(SessionFactory.class);
    this.session = app.require(Session.class);
    this.metadataUtils = HibernateMetadataUtils.getInstanceForSessionFactory(sessionFactory);
    try {
      persistentClass = (Class<T>) ReflectionUtils.getTypeArguments(CrudRepositoryImpl.class, this.getClass()).get(0);
    } catch (Throwable e) {
      logger.error("Error to detect crud class.", e);
    }
  }

  /**
   * Gets the EntityManager sessionFactory
   *
   * @return EntityManager sessionFactory
   */
  protected EntityManagerFactory getEntityManagerFactory() {
    return entityManagerFactory;
  }

  /**
   * Gets the Hibernate sessionFactory
   *
   * @return Hibernate sessionFactory
   */
  protected SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  /**
   * Gets the current Entity manager
   *
   * @return current Entity manager
   */
  protected EntityManager getEntityManager() {
    return getEntityManagerFactory().createEntityManager();
  }

  /**
   * Gets the current Hibernate session
   *
   * @return current Hibernate session
   */
  protected Session getSession() {
    return ((getSessionFactory().getCurrentSession() != null) && getSessionFactory().getCurrentSession().getTransaction().isActive())
        ? getSessionFactory().getCurrentSession()
        : session;
  }

  /**
   * Get the instance of HibernateMetadataUtils associated with the session factory.
   *
   * @return current state of metadataUtils
   */
  protected HibernateMetadataUtils getMetadataUtils() {
    return metadataUtils;
  }

  // Public methods

  public void delete(@Nonnull T entity) {
    _delete(entity);
  }

  public void deleteAll(@Nonnull Iterable<? extends T> entities) {
    _delete(entities);
  }

  public void deleteById(@Nonnull ID id) {
    _deleteById(persistentClass, id);
  }

  public void deleteByIds(@Nonnull Iterable<? extends ID> ids) {
    _deleteById(persistentClass, ids);
  }

  public boolean existsById(@Nonnull ID id) {
    return _existsById(persistentClass, id);
  }

  public @Nonnull List<T> findAll() {
    return _all(persistentClass);
  }

  public @Nonnull Optional<T> findById(@Nonnull ID id) {
    return Optional.ofNullable(_get(persistentClass, id));
  }

  public @Nonnull List<T> findByIds(@Nonnull Iterable<ID> ids) {
    return _get(persistentClass, ids);
  }

  public @Nonnull T getReference(@Nonnull ID id) {
    return _load(persistentClass, id);
  }

  public @Nonnull List<T> getReferences(@Nonnull Iterable<ID> ids) {
    return _load(persistentClass, ids);
  }

  public @Nonnull T[] getReferences(@Nonnull Serializable... ids) {
    return _load(persistentClass, ids);
  }

  public @Nonnull T save(@Nonnull T entity) {
    return _saveOrUpdateIsNew(entity);
  }

  public @Nonnull List<T> saveAll(@Nonnull Iterable<T> entities) {
    return _saveOrUpdateIsNew(entities);
  }

  public @Nonnull T update(@Nonnull T entity) {
    return _saveOrUpdateIsNew(entity);
  }

  // Protected methods

  protected void _delete(Object entity) {
    if (entity != null) {
      Serializable id = getMetadataUtils().getId(entity);
      if (id != null) {
        entity = getSession().get(getMetadataUtils().getUnproxiedClass(entity), id);
        if (entity != null) {
          getSession().delete(entity);
        }
      }
    }
  }

  protected void _delete(@Nonnull Iterable<? extends T> entities) {
    for (Object entity : entities) {
      if (entity != null) {
        getSession().delete(entity);
      }
    }
  }

  protected void _deleteById(Class<?> type, Serializable id) {
    if (id != null) {
      type = getMetadataUtils().getUnproxiedClass(type);
      Object entity = getSession().get(type, id);
      if (entity != null) {
        getSession().delete(entity);
      }
    }
  }

  protected void _deleteById(Class<?> type, @Nonnull Iterable<? extends ID> ids) {
    type = getMetadataUtils().getUnproxiedClass(type);
    /*
     * Criteria c = getSession().createCriteria(type);
     * c.add(Restrictions.in("id", ids));
     * for (Object entity : c.list()) {
     *     getSession().delete(entity);
     * }
     */
    CriteriaBuilder cb = getSession().getCriteriaBuilder();
    CriteriaQuery<?> query = cb.createQuery(type);
    Root<?> root = query.from(type);
    query.where(cb.equal(root.get("id"), Restrictions.in("id", ids)));
    List<?> list = getSession().createQuery(query).getResultList();
    for (Object entity : list) {
      getSession().delete(entity);
    }
  }

  protected boolean _exists(Object entity) {
    if (getSession().contains(entity)) {
      return true;
    }

    return _existsById(entity.getClass(), getMetadataUtils().getId(entity));
  }

  protected boolean _existsById(Class<?> type, Serializable id) {
    if (type == null) {
      throw new NullPointerException("Type is null.");
    }

    if (id == null) {
      return false;
    }

    type = getMetadataUtils().getUnproxiedClass(type);

    return getSession()
        .createQuery("select e.id from " + getMetadataUtils().get(type).getEntityName() + " e where e.id = :id")
        .setParameter("id", id)
        .getResultList().size() == 1;
  }

  protected List<T> _all(Class<T> type) {
    type = getMetadataUtils().getUnproxiedClass(type);
    return getSession()
        .createQuery("select e from " + type.getSimpleName() + " e order by e.id", type)
        .getResultList();
  }

  protected T _get(Class<T> type, Serializable id) {
    type = getMetadataUtils().getUnproxiedClass(type);
    return getSession().get(type, id);
  }

  protected List<T> _get(Class<T> type, @Nonnull Iterable<ID> ids) {
    type = getMetadataUtils().getUnproxiedClass(type);
    return getSession()
        .createQuery("select e from " + type.getSimpleName() + " e where e.id in :ids", type)
        .setParameter("ids", ids)
        .getResultList();
  }

  /**
   * Returns the persistent instance of the given entity class with the given identifier,
   * assuming that the instance exists. Throw an unrecoverable exception if there is no
   * matching database row. If the class is mapped with a proxy, {@code load()} just returns
   * an uninitialized proxy and does not actually hit the database until you invoke a method
   * of the proxy. This behaviour is very useful if you wish to create an association to an
   * object without actually loading it from the database. It also allows multiple instances
   * to be loaded as a batch if batch-size is defined for the class mapping.
   */
  protected T _load(Class<T> type, Serializable id) {
    type = getMetadataUtils().getUnproxiedClass(type); //Get the real entity class
    return getSession().load(type, id);
  }

  /**
   * Returns the persistent instance of the given entity class with the given identifier,
   * assuming that the instance exists. Throw an unrecoverable exception if there is no
   * matching database row. An array of entities is returned that matches the same order
   * of the ids listed in the call. For each entity that is not found in the datastore,
   * a null will be inserted in its place in the return array.
   *
   * @see #_load(Class, Serializable)
   */
  protected List<T> _load(Class<T> type, @Nonnull Iterable<ID> ids) {
    type = getMetadataUtils().getUnproxiedClass(type); //Get the real entity class
    List<T> retVal = new ArrayList<>();
    for (ID id : ids) {
      retVal.add(_load(type, id));
    }
    return retVal;
  }

  /**
   * Returns the persistent instance of the given entity class with the given identifier,
   * assuming that the instance exists. Throw an unrecoverable exception if there is no
   * matching database row. An array of entities is returned that matches the same order
   * of the ids listed in the call. For each entity that is not found in the datastore,
   * a null will be inserted in its place in the return array.
   *
   * @see #_load(Class, Serializable)
   */
  @SuppressWarnings("unchecked")
  protected T[] _load(Class<T> type, @Nonnull Serializable... ids) {
    type = getMetadataUtils().getUnproxiedClass(type); //Get the real entity class
    Object[] retVal = (Object[]) Array.newInstance(type, ids.length);
    for (int i = 0; i < ids.length; i++) {
      if (ids[i] != null)
        retVal[i] = _load(type, ids[i]);
    }
    return (T[]) retVal;
  }

  /**
   * Read the persistent state associated with the given identifier into the given transient
   * instance. Throw an unrecoverable exception if there is no matching database row.
   */
  protected void _load(Object transientEntity, Serializable id) {
    getSession().load(transientEntity, id);
  }

  protected Serializable _save(T entity) {
    return getSession().save(entity);
  }

  protected T _saveOrUpdateIsNew(T entity) {
    if (entity == null)
      throw new IllegalArgumentException("Attempt to saveOrUpdate with null entity!");

    Serializable id = getMetadataUtils().getId(entity);

    if (id == null || (Long.valueOf(0)).equals(id) || !_exists(entity)) {
      Serializable persistentEntity = _save(entity);
      return _get(persistentClass, persistentEntity);
    } else {
      _update(entity);
      return _get(persistentClass, getMetadataUtils().getId(entity));
    }
  }

  protected @Nonnull List<T> _saveOrUpdateIsNew(@Nonnull Iterable<T> entities) {

    List<T> persistenceEntities = new ArrayList<>();

    for (T entity : entities) {
      T persistenceEntity = _saveOrUpdateIsNew(entity);
      persistenceEntities.add(persistenceEntity);
    }

    return persistenceEntities;
  }

  protected void _saveOrUpdate(T entity) {
    getSession().saveOrUpdate(entity);
  }

  protected void _update(T entity) {
    getSession().update(entity);
  }

}
