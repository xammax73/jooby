package io.xammax.jooby.hibernate;

import io.xammax.jooby.internal.hibernate.Metadata;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metamodel.spi.MetamodelImplementor;
import org.hibernate.type.CollectionType;
import org.hibernate.type.Type;

import java.io.Serializable;

public class HibernateEntityMetadata implements Metadata {

  private SessionFactory sessionFactory;
  private ClassMetadata metadata;
  private Class<?> collectionType;

  public HibernateEntityMetadata(SessionFactory sessionFactory, ClassMetadata classMetadata, Class<?> collectionType) {
    this.sessionFactory = sessionFactory;
    this.metadata = classMetadata;
    this.collectionType = collectionType;
  }

  public String getEntityName() {
    return metadata.getEntityName();
  }

  public String getIdProperty() {
    return metadata.getIdentifierPropertyName();
  }

  public Metadata getIdType() {
    return new HibernateNonEntityMetadata(sessionFactory, metadata.getIdentifierType(), null);
  }

  public Serializable getIdValue(Object object) {
    return metadata.getIdentifier(object, (SharedSessionContractImplementor) null);
  }

  public Class<?> getJavaClass() {
    return metadata.getMappedClass();
  }

  public String[] getProperties() {
    String[] pn = metadata.getPropertyNames();
    String[] result = new String[pn.length + 1];
    result[0] = metadata.getIdentifierPropertyName();
    System.arraycopy(pn, 0, result, 1, pn.length);
    return result;
  }

  public Metadata getPropertyType(String property) {
    Type pType = metadata.getPropertyType(property);
    Class<?> pCollectionType = null;

    if (pType.isCollectionType()) {
      pType = ((CollectionType)pType).getElementType((SessionFactoryImplementor) sessionFactory);
      pCollectionType = pType.getReturnedClass();
    }

    if (pType.isEntityType()) {
      MetamodelImplementor metamodel = (MetamodelImplementor) sessionFactory.getMetamodel();
      ClassMetadata classMetadata = (ClassMetadata) metamodel.entityPersister((pType).getName());
      return new HibernateEntityMetadata(sessionFactory, classMetadata, pCollectionType);
    } else {
      return new HibernateNonEntityMetadata(sessionFactory, pType, pCollectionType);
    }
  }

  public Object getPropertyValue(Object object, String property) {
    if (getIdProperty().equals(property))
      return getIdValue(object);
    else
      return metadata.getPropertyValue(object, property);
  }

  public boolean isCollection() {
    return collectionType != null;
  }

  public Class<?> getCollectionClass() {
    return collectionType;
  }

  public boolean isEmbeddable() {
    return false;
  }

  public boolean isEntity() {
    return true;
  }

  public boolean isNumeric() {
    return false;
  }

  public boolean isString() {
    return false;
  }

}
