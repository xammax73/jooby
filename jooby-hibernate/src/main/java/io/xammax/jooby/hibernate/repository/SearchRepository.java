package io.xammax.jooby.hibernate.repository;

import io.xammax.jooby.hibernate.model.Persistable;
import io.xammax.jooby.internal.hibernate.ImmutableSearch;
import io.xammax.jooby.internal.hibernate.SearchResult;

import java.io.Serializable;
import java.util.List;

/**
 * Interface for a repository that can be used for a single specified type model object. A single
 * instance implementing this interface can be used only for the type of model object specified
 * in the type parameters.
 *
 * @param <T> The type of the model object for which this instance is to be used.
 * @param <ID> The type of the id of the model object for which this instance is to be used.
 */
public interface SearchRepository<T extends Persistable<ID>, ID extends Serializable> extends CrudRepository<T, ID> {

  /**
   * Returns the number of entities available.
   *
   * @return the number of entities.
   */
  long count();

  /**
   * Returns the total number of results that would be returned using the given search if there
   * were no pagination or max results limits.
   *
   * @param search the search parameters in the {@literal ImmutableSearch} object.
   * @return general quantity of objects.
   */
  long count(ImmutableSearch search);

  /**
   * Returns list of entities for the given search parameters.
   *
   * @param search the search parameters in the {@code ImmutableSearch} object.
   * @return the result type is automatically determined by the context in which the method is
   *         called.
   */
  <RT> List<RT> search(ImmutableSearch search);

  /**
   * Returns a single entity using the given parameters.
   *
   * @param search the search parameters in the {@literal ImmutableSearch} object.
   * @return the result type is automatically determined by the context in which the method is
   *         called.
   */
  <RT> RT searchUnique(ImmutableSearch search);

  /**
   * Returns a {@literal SearchResult} object that includes both the list of results like
   * {@code findBy()} and the total length like {@code count()}.
   *
   * @param search the search parameters in the {@literal ImmutableSearch} object.
   * @return the result type is automatically determined by the context in which the method is
   *         called.
   */
  <RT> SearchResult<RT> searchAndCount(ImmutableSearch search);

}
