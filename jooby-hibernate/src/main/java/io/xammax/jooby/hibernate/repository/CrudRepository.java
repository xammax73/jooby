package io.xammax.jooby.hibernate.repository;

import io.xammax.jooby.hibernate.model.Persistable;
import org.hibernate.HibernateException;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Interface for generic CRUD operations on a repository for a specific type.
 */
public interface CrudRepository<T extends Persistable<ID>, ID extends Serializable> {

  /**
   * Deletes the given entity.
   *
   * @param entity must not be {@literal null}
   * @throws IllegalArgumentException if the given {@literal entity} is {@literal null}
   */
  void delete(@Nonnull T entity);

  /**
   * Deletes all of the given entities.
   *
   * @param entities must not be {@literal null} nor contain {@literal null} elements
   * @throws IllegalArgumentException if {@literal entities} or one of {@literal entities} is
   *         {@literal null}
   */
  void deleteAll(@Nonnull Iterable<? extends T> entities);

  /**
   * Deletes the entity with the given id.
   *
   * @param id must not be {@literal null}
   * @throws IllegalArgumentException if the given {@literal id} is {@literal null}
   */
  void deleteById(@Nonnull ID id);

  /**
   * Deletes all instances of the type {@code T} with the given IDs.
   *
   * @param ids must not be {@literal null} nor contain {@literal null} elements
   * @throws IllegalArgumentException if the given {@literal ids} or one of {@literal ids} is
   *         {@literal null}
   */
  void deleteByIds(@Nonnull Iterable<? extends ID> ids);

  /**
   * Returns whether an entity with the given id exists.
   *
   * @param id must not be {@literal null}
   * @return {@literal true} if an entity with the given id exists, {@literal false} otherwise
   * @throws IllegalArgumentException if {@literal id} is {@literal null}
   */
  boolean existsById(@Nonnull ID id);

  /**
   * Returns all instances of the type {@code T}.
   *
   * @return all of entity type or {@literal Optional#empty()} if none found
   */
  @Nonnull List<T> findAll();

  /**
   * Returns an entity by the given id.
   *
   * @param id must not be {@literal null}
   * @return the entity with the given id or {@literal Optional#empty()} if entity not found
   * @throws IllegalArgumentException if {@literal id} is {@literal null}
   */
  @Nonnull Optional<T> findById(@Nonnull ID id);

  /**
   * Returns all instances of the type {@code T} with the given IDs. If some or all ids are not
   * found, no entities are returned for these IDs. The order of elements in the result is not
   * guaranteed.
   *
   * @param ids must not be {@literal null} nor contain any {@literal null} values
   * @return guaranteed to be not {@literal null}. The size is equal or less than the number of
   *         the given {@literal ids}
   * @throws IllegalArgumentException in case the given {@link Iterable ids} or one of its items
   *         is {@literal null}
   */
  @Nonnull List<T> findByIds(@Nonnull Iterable<ID> ids);

  /**
   * Returns a reference to the entity with the specified type and id. This does not require
   * a call to the datastore and does not populate any of the entity's values. Values may be
   * fetched lazily at a later time. This increases performance if a another entity is being
   * saved that should reference this entity but the values of this entity are not needed.
   *
   * @param id the ID of the entity to return. Must not be {@literal null}
   * @return the entity with the given id or {@literal Optional#empty()} if none found
   * @throws HibernateException if no matching entity is found
   */
  @Nonnull T getReference(@Nonnull ID id);

  /**
   * Returns a reference to the entities of the specified type with the given ids from the
   * datastore. This does not require a call to the datastore and does not populate any of the
   * entities' values. Values may be fetched lazily at a later time. This increases performance
   * if a another entity is being saved that should reference these entities but the values of
   * these entities are not needed.
   *
   * @param ids the IDs of the entity to retrieve. Must not be {@literal null}
   * @return all of entity type with the {@literal ids} or {@literal Iterable#empty()} if none
   *         found
   * @throws HibernateException if any of the matching entities are not found
   */
  @Nonnull List<T> getReferences(@Nonnull Iterable<ID> ids);

  /**
   * Returns a reference to the entities of the specified type with the given ids from the
   * datastore. This does not require a call to the datastore and does not populate any of the
   * entities' values. Values may be fetched lazily at a later time. This increases performance
   * if a another entity is being saved that should reference these entities but the values of
   * these entities are not needed.
   *
   * @param ids the IDs of the entity to retrieve. Must not be {@literal null}
   * @return all of entity type with the {@literal ids} or {@literal Iterable#empty()} if none
   *         found
   * @throws HibernateException if any of the matching entities are not found
   */
  @Nonnull T[] getReferences(@Nonnull Serializable... ids);

  /**
   * Saves a given entity. The save operation might have changed the entity instance completely.
   *
   * @param entity must not be {@literal null}
   * @return the saved entity; will never be {@literal null}
   * @throws IllegalArgumentException if the given {@literal entity} is {@literal null}
   */
  @Nonnull T save(@Nonnull T entity);

  /**
   * Saves all given entities.
   *
   * @param entities must not be {@literal null} nor must it contain {@literal null}
   * @return the saved entities; will never be {@literal null}. The returned {@link List} will
   *         have the same size as the {@link Iterable} passed as an argument
   * @throws IllegalArgumentException if the given {@link Iterable} {@literal entities} or one
   *         of the given {@literal entities} is {@literal null}
   */
  @Nonnull List<T> saveAll(@Nonnull Iterable<T> entities);

  /**
   * Updates a given entity. This method issues an explicit update for the given entity. The
   * method differs from {@link #save(T)} in that an update will be generated regardless if
   * the entity has been saved previously or not. If the entity has no assigned ID then an
   * exception will be thrown.
   *
   * @param entity must not be {@literal null}
   * @return the updated entity; will never be {@literal null}
   * @throws IllegalArgumentException in case the given {@literal entity} is {@literal null}
   */
  @Nonnull T update(@Nonnull T entity);

}
