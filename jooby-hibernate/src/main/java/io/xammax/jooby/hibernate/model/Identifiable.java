package io.xammax.jooby.hibernate.model;

import javax.annotation.Nullable;
import java.io.Serializable;

/**
 * Defines a class as identifiable by an identifier.
 *
 * @param <ID> the type of the identifier
 */
public interface Identifiable<ID extends Serializable> {

  /**
   * Gets the type of the identifier of entity.
   *
   * @return the identifier of entity. Can be {@literal null}
   */
  @Nullable ID getId();

}
