package io.xammax.jooby.internal.hibernate;

import javax.annotation.Nullable;
import java.io.Serializable;

public class Page implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final Long UNSPECIFIED_FIRST_RESULT = -1L;
  private static final Integer UNSPECIFIED_MAX_RESULTS = -1;
  private static final Integer UNSPECIFIED_PAGE_NUMBER = -1;

  private static final Long DEFAULT_FIRST_RESULT = 0L;
  private static final Integer DEFAULT_MAX_RESULTS = 25;
  private static final Integer DEFAULT_PAGE_NUMBER = 0;

  /**
   * Returns zero based index of first result record.
   */
  private Long firstResult;

  /**
   * Returns the maximum number of records.
   */
  private Integer maxResults;

  /**
   * Returns zero based index of the page.
   */
  private Integer pageNumber;

  /* *
   * Returns the total quantity of pages.
   */
  //private Integer totalPages;

  /* *
   * Returns the total quantity of all elements.
   */
  //private Long totalElements;

  /**
   * Creates a new {@link Page} instance.
   */
  public Page() {
    this(null, null, null);
  }

  /**
   * Creates a new {@link Page} instance.
   *
   * @param firstResult the first result value of items
   * @param maxResults the quantity of items on the page
   * @param pageNumber the number of pages
   */
  public Page(@Nullable Long firstResult, @Nullable Integer maxResults, @Nullable Integer pageNumber) {
    this.maxResults = maxResults == null ?
        DEFAULT_MAX_RESULTS : (maxResults <= UNSPECIFIED_MAX_RESULTS ? UNSPECIFIED_MAX_RESULTS : maxResults < 1 ? DEFAULT_MAX_RESULTS : maxResults);
    this.pageNumber = pageNumber == null ?
        DEFAULT_PAGE_NUMBER : (pageNumber <= UNSPECIFIED_PAGE_NUMBER ? UNSPECIFIED_PAGE_NUMBER : pageNumber);

    if (firstResult == null) {
      this.firstResult = (this.maxResults.equals(UNSPECIFIED_MAX_RESULTS) || this.pageNumber.equals(UNSPECIFIED_PAGE_NUMBER)) ?
          DEFAULT_FIRST_RESULT : (long) this.maxResults * (long) this.pageNumber;
    }
    else {
      this.firstResult = firstResult <= UNSPECIFIED_FIRST_RESULT ? UNSPECIFIED_FIRST_RESULT : firstResult;
    }
  }

  /**
   * Gets zero based index of first result record to return.
   *
   * @return number of first record
   */
  public Long getFirstResult() {
    return firstResult;
  }

  public void setFirstResult(Long firstResult) {
    this.firstResult = firstResult;
  }

  /**
   * Gets the maximum number of records to return. Also used as page size when calculating the
   * first record to return based {@literal maxResults} on page. If value {@code maxResults=0},
   * the value is unspecified.
   *
   * @return maximum number of records on page
   */
  public Integer getMaxResults() {
    return maxResults;
  }

  /**
   * Gets zero based number of the page to return ({@literal pageNumber}). The size of page
   * is determined by {@literal maxResults}. If both parameters {@literal pageNumber} and
   * {@literal maxResults} are specified (i.e. > 0), the {@literal firstResult} returned is
   * calculated as {@code pageNumber * maxResults}. The {@literal firstResult} has precedence
   * over {@literal pageNumber}. If {@literal firstResult} is specified (i.e. > 0),
   * {@literal pageNumber} is ignored.
   */
  public void setMaxResults(Integer maxResults) {
    this.maxResults = maxResults;
  }

  public Integer getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(Integer pageNumber) {
    this.pageNumber = pageNumber;
  }

//    public Integer getTotalPages() {
//        return getPageSize() == 0 ? DEFAULT_PAGE_SIZE : (int) Math.ceil((double) total / (double) getPageSize());
//    }

}
