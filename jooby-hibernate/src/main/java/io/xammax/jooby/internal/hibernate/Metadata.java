package io.xammax.jooby.internal.hibernate;

import java.io.Serializable;

/**
 * This interface provides meta data for a single persistable type. For get meta data instances
 * can be used {@link MetadataUtils#get(Class)} or {@link MetadataUtils#get(Class, String)}. This
 * interface provides a layer of abstraction between the framework and the underlying JPA provider
 * (ex. Hibernate). By switching out the implementation of this interface, the framework should be
 * able to be used with different JPA providers.
 */
public interface Metadata {

  /**
   * Returns {@literal true} if the type is an entity.
   */
  boolean isEntity();

  /**
   * Returns {@literal true} if the type is an embeddable class (for Hibernate).
   */
  boolean isEmbeddable();

  /**
   * Returns {@literal true} if the type is a collection.
   */
  boolean isCollection();

  /**
   * Returns {@literal true} if the type is persisted as a string (char or varchar).
   */
  boolean isString();

  /**
   * Returns {@literal true} if the type is a number. For example:
   *
   * <p><pre class="code>
   *     int, long, Float, Number, Double, etc.
   * <pre/>
   */
  boolean isNumeric();

  /**
   * Returns the Java class of this type. If the type is a collection, returns the type of the
   * collection elements.
   */
  Class<?> getJavaClass();

  /**
   * Returns the entity name if the type of object is an entity. Otherwise throw an
   * {@link UnsupportedOperationException}.
   */
  String getEntityName();

  /**
   * Returns an {@link String} array of the names of all properties that this type has, if any.
   * Returns {@literal null} if this a simple value type with no properties.
   */
  String[] getProperties();

  /**
   * Returns the value of the given {@literal property} of the given {@literal object} of this
   * type. Returns {@literal null} if this is a simple value type with no properties.
   */
  Object getPropertyValue(Object object, String property);

  /**
   * Returns the metadata for the given {@literal property} of this type. Returns {@literal null}
   * if this is a simple value type with no properties.
   */
  Metadata getPropertyType(String property);

  /**
   * Returns the name of the id property of this type. Returns {@literal null} if this is not
   * an entity type.
   */
  String getIdProperty();

  /**
   * Returns the metadata for the id property of this type. Returns {@literal null} if this is
   * not an entity type.
   */
  Metadata getIdType();

  /**
   * Returns the value of the id property of the given {@literal object} of this type. Returns
   * {@literal null} if this is not an entity type.
   */
  Serializable getIdValue(Object object);

  /**
   * If the type is a collection, return the Java class of the collection itself, not the Java
   * class of it's elements as with {@link #getJavaClass()}. For example:
   *
   * <p><pre class="code>
   *     ArrayList<Project>,
   *     Set<Person>,
   *     String[].
   * <pre/>
   */
  Class<?> getCollectionClass();

}
