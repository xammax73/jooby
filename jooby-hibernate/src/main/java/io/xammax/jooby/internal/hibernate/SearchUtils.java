package io.xammax.jooby.internal.hibernate;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Utilities for working with {@link ImmutableSearch} and {@link MutableSearch} interfaces.
 */
public class SearchUtils {

  /* Add Fetches */

  public static void addFetch(MutableSearch search, String property) {
    if (property == null || property.equals(""))
      return; // null properties do nothing, don't bother to add them.
    List<String> fetches = search.getFetches();
    if (fetches == null) {
      fetches = new ArrayList<>();
      search.setFetches(fetches);
    }
    fetches.add(property);
  }

  public static void addFetches(MutableSearch search, String... properties) {
    if (properties != null) {
      for (String property : properties) {
        addFetch(search, property);
      }
    }
  }

  /* Add Fields */

  public static void addField(@Nonnull MutableSearch search, Field field) {
    List<Field> fields = search.getFields();
    if (fields == null) {
      fields = new ArrayList<>();
      search.setFields(fields);
    }
    fields.add(field);
  }

  public static void addFields(MutableSearch search, Field... fields) {
    if (fields != null) {
      for (Field field : fields) {
        addField(search, field);
      }
    }
  }

  /**
   * If this field is used with {@code resultMode == RESULT_MAP}, the {@literal property} will
   * also be used as the key for this value in the map.
   */
  public static void addField(MutableSearch search, String property) {
    if (property == null)
      return; // null properties do nothing, don't bother to add them.
    addField(search, new Field(property));
  }

  /**
   * If this field is used with {@code resultMode == RESULT_MAP}, the {@literal key} will be
   * used as the key for this value in the map.
   */
  public static void addField(MutableSearch search, String property, String key) {
    if (property == null || key == null)
      return; // null properties do nothing, don't bother to add them.
    addField(search, new Field(property, key));
  }

  /**
   * If this field is used with {@code resultMode == RESULT_MAP}, the {@literal property} will
   * also be used as the key for this value in the map.
   */
  public static void addField(MutableSearch search, String property, FieldOperator operator) {
    if (property == null)
      return; // null properties do nothing, don't bother to add them.
    addField(search, new Field(property, operator));
  }

  /**
   * If this field is used with {@code resultMode == RESULT_MAP}, the {@literal key} will be
   * used as the key for this value in the map.
   */
  public static void addField(MutableSearch search, String property, String key, FieldOperator operator) {
    if (property == null || key == null)
      return; // null properties do nothing, don't bother to add them.
    addField(search, new Field(property, key, operator));
  }

  /* Add Filters */

  public static void addFilter(@Nonnull MutableSearch search, Filter filter) {
    List<Filter> filters = search.getFilters();
    if (filters == null) {
      filters = new ArrayList<>();
      search.setFilters(filters);
    }
    filters.add(filter);
  }

  public static void addFilters(MutableSearch search, Filter... filters) {
    if (filters != null) {
      for (Filter filter : filters) {
        addFilter(search, filter);
      }
    }
  }

  /**
   * Add a filter that uses the {@literal ALL} operator.
   */
  public static void addFilterAll(MutableSearch search, String property, Filter filter) {
    addFilter(search, Filter.all(property, filter));
  }

  /**
   * Add a filter that uses the {@literal AND} operator.
   */
  public static void addFilterAnd(MutableSearch search, Filter... filters) {
    addFilter(search, Filter.and(filters));
  }

  /**
   * Add a filter that uses the {@literal IS EMPTY} operator.
   */
  public static void addFilterEmpty(MutableSearch search, String property) {
    addFilter(search, Filter.isEmpty(property));
  }

  /**
   * Add a filter that uses the {@literal ==} operator.
   */
  public static void addFilterEqual(MutableSearch search, String property, Object value) {
    addFilter(search, Filter.equal(property, value));
  }

  /**
   * Add a filter that uses the {@literal >=} operator.
   */
  public static void addFilterGreaterOrEqual(MutableSearch search, String property, Object value) {
    addFilter(search, Filter.greaterOrEqual(property, value));
  }

  /**
   * Add a filter that uses the {@literal >} operator.
   */
  public static void addFilterGreaterThan(MutableSearch search, String property, Object value) {
    addFilter(search, Filter.greaterThan(property, value));
  }

  /**
   * Add a filter that uses the {@literal ILIKE} operator.
   */
  public static void addFilterILike(MutableSearch search, String property, String value) {
    addFilter(search, Filter.ilike(property, value));
  }

  /**
   * Add a filter that uses the {@literal IN} operator.
   */
  public static void addFilterIn(MutableSearch search, String property, Collection<?> value) {
    addFilter(search, Filter.in(property, value));
  }

  /**
   * Add a filter that uses the {@literal IN} operator.
   */
  public static void addFilterIn(MutableSearch search, String property, Object... value) {
    addFilter(search, Filter.in(property, value));
  }

  /**
   * Add a filter that uses the {@literal <=} operator.
   */
  public static void addFilterLessOrEqual(MutableSearch search, String property, Object value) {
    addFilter(search, Filter.lessOrEqual(property, value));
  }

  /**
   * Add a filter that uses the {@literal <} operator.
   */
  public static void addFilterLessThan(MutableSearch search, String property, Object value) {
    addFilter(search, Filter.lessThan(property, value));
  }

  /**
   * Add a filter that uses the {@literal LIKE} operator.
   */
  public static void addFilterLike(MutableSearch search, String property, String value) {
    addFilter(search, Filter.like(property, value));
  }

  /**
   * Add a filter that uses the {@literal NONE} operator.
   */
  public static void addFilterNone(MutableSearch search, String property, Filter filter) {
    addFilter(search, Filter.none(property, filter));
  }

  /**
   * Add a filter that uses the {@literal NOT} operator.
   */
  public static void addFilterNot(MutableSearch search, Filter filter) {
    addFilter(search, Filter.not(filter));
  }

  /**
   * Add a filter that uses the {@literal !=} operator.
   */
  public static void addFilterNotEqual(MutableSearch search, String property, Object value) {
    addFilter(search, Filter.notEqual(property, value));
  }

  /**
   * Add a filter that uses the {@literal NOT IN} operator.
   */
  public static void addFilterNotIn(MutableSearch search, String property, Collection<?> value) {
    addFilter(search, Filter.notIn(property, value));
  }

  /**
   * Add a filter that uses the {@literal NOT IN} operator.
   */
  public static void addFilterNotIn(MutableSearch search, String property, Object... value) {
    addFilter(search, Filter.notIn(property, value));
  }

  /**
   * Add a filter that uses the {@literal IS NOT EMPTY} operator.
   */
  public static void addFilterNotEmpty(MutableSearch search, String property) {
    addFilter(search, Filter.isNotEmpty(property));
  }

  /**
   * Add a filter that uses the {@literal IS NOT NULL} operator.
   */
  public static void addFilterNotNull(MutableSearch search, String property) {
    addFilter(search, Filter.isNotNull(property));
  }

  /**
   * Add a filter that uses the {@literal IS NULL} operator.
   */
  public static void addFilterNull(MutableSearch search, String property) {
    addFilter(search, Filter.isNull(property));
  }

  /**
   * Add a filter that uses the {@literal OR} operator.
   */
  public static void addFilterOr(MutableSearch search, Filter... filters) {
    addFilter(search, Filter.or(filters));
  }

  /**
   * Add a filter that uses the {@literal SOME} operator.
   */
  public static void addFilterSome(MutableSearch search, String property, Filter filter) {
    addFilter(search, Filter.some(property, filter));
  }

  /* Add Sorts */

  public static void addSort(MutableSearch search, Sort sort) {
    if (sort == null)
      return;
    List<Sort> sorts = search.getSorts();
    if (sorts == null) {
      sorts = new ArrayList<>();
      search.setSorts(sorts);
    }
    sorts.add(sort);
  }

  public static void addSorts(MutableSearch search, Sort... sorts) {
    if (sorts != null) {
      for (Sort sort : sorts) {
        addSort(search, sort);
      }
    }
  }

  /**
   * Add sort by property. {@literal direction} can be {@literal null}.
   * Ascending if {@code direction == ASC}, descending if {@code direction == DESC}.
   */
  public static void addSort(MutableSearch search, String property, Direction direction) {
    addSort(search, property, direction, false);
  }

  /**
   * Add sort by property. {@literal direction} can be {@literal null}.
   * Ascending if {@code direction == ASC}, descending if {@code direction == DESC}.
   */
  public static void addSort(MutableSearch search, String property, Direction direction, Boolean ignoreCase) {
    if (property == null)
      return; // null properties do nothing, don't bother to add them.
    addSort(search, new Sort(property, direction, ignoreCase));
  }

  /**
   * Add ascending sort by property.
   */
  public static void addSortAsc(MutableSearch search, String property) {
    addSort(search, property, Direction.ASC, false);
  }

  /**
   * Add ascending sort by property.
   */
  public static void addSortAsc(MutableSearch search, String property, Boolean ignoreCase) {
    addSort(search, property, Direction.ASC, ignoreCase);
  }

  /**
   * Add descending sort by property.
   */
  public static void addSortDesc(MutableSearch search, String property) {
    addSort(search, property, Direction.DESC, false);
  }

  /**
   * Add descending sort by property.
   */
  public static void addSortDesc(MutableSearch search, String property, Boolean ignoreCase) {
    addSort(search, property, Direction.DESC, ignoreCase);
  }

  /* Remove Fetches */

  public static void removeFetch(@Nonnull MutableSearch search, String property) {
    if (search.getFetches() != null)
      search.getFetches().remove(property);
  }

  /* Remove Fields */

  /**
   * Remove current field.
   */
  public static void removeField(@Nonnull MutableSearch search, Field field) {
    if (search.getFields() != null)
      search.getFields().remove(field);
  }

  /**
   * Remove all fields on the given property.
   */
  public static void removeField(@Nonnull MutableSearch search, String property) {
    if (search.getFields() == null)
      return;
    search.getFields().removeIf(field -> field.getProperty().equals(property));
  }

  /**
   * Remove all fields on the given property and key.
   */
  public static void removeField(@Nonnull MutableSearch search, String property, String key) {
    if (search.getFields() == null)
      return;
    search.getFields().removeIf(field -> field.getProperty().equals(property) && field.getKey().equals(key));
  }

  /* Remove Filters */

  /**
   * Remove current filter.
   */
  public static void removeFilter(@Nonnull MutableSearch search, Filter filter) {
    List<Filter> filters = search.getFilters();
    if (filters != null)
      filters.remove(filter);
  }

  /**
   * Remove all filters on the given property.
   */
  public static void removeFiltersOnProperty(MutableSearch search, String property) {
    if (property == null || search.getFilters() == null)
      return;
    search.getFilters().removeIf(filter -> property.equals(filter.getProperty()));
  }

  /* Remove Sorts */

  /**
   * Remove current sort.
   */
  public static void removeSort(@Nonnull MutableSearch search, Sort sort) {
    if (search.getSorts() != null)
      search.getSorts().remove(sort);
  }

  /**
   * Remove all sorts on the given property.
   */
  public static void removeSort(MutableSearch search, String property) {
    if (property == null || search.getSorts() == null)
      return;
    search.getSorts().removeIf(sort -> property.equals(sort.getProperty()));
  }

  /* Clear functions */

  /**
   * Clear all search parameters.
   *
   * @param search current search.
   */
  public static void clear(MutableSearch search) {
    clearFilters(search);
    clearSorts(search);
    clearFields(search);
    clearPaging(search);
    clearFetches(search);
    clearJoins(search);
    search.setResultMode(Result.RESULT_AUTO);
    search.setDisjunction(false);
  }

  public static void clearFetches(@Nonnull MutableSearch search) {
    if (search.getFetches() != null)
      search.getFetches().clear();
  }

  public static void clearFields(@Nonnull MutableSearch search) {
    if (search.getFields() != null)
      search.getFields().clear();
  }

  public static void clearFilters(@Nonnull MutableSearch search) {
    if (search.getFilters() != null)
      search.getFilters().clear();
  }

  public static void clearJoins(@Nonnull MutableSearch search) {
    if (search.getJoins() != null)
      search.getJoins().clear();
  }

  public static void clearPaging(@Nonnull MutableSearch search) {
    search.setFirstResult(-1);
    search.setMaxResults(-1);
    search.setPageNumber(-1);
  }

  public static void clearSorts(@Nonnull MutableSearch search) {
    if (search.getSorts() != null)
      search.getSorts().clear();
  }

  /* Merge Fetches */

  /**
   * Modify the search by adding the given fetches to the current fetches.
   */
  public static void mergeFetches(@Nonnull MutableSearch search, List<String> fetches) {
    List<String> list = search.getFetches();
    if (list == null) {
      list = new ArrayList<>();
      search.setFetches(list);
    }
    for (String fetch : fetches) {
      if (!list.contains(fetch)) {
        list.add(fetch);
      }
    }
  }

  /**
   * Modify the search by adding the given fetches to the current fetches.
   */
  public static void mergeFetches(MutableSearch search, String... fetches) {
    mergeFetches(search, Arrays.asList(fetches));
  }

  /* Merge Fields */

  /**
   * Modify the search by adding the given fields before the current fields.
   */
  public static void mergeFieldsBefore(@Nonnull MutableSearch search, List<Field> fields) {
    List<Field> list = search.getFields();
    if (list == null) {
      list = new ArrayList<>();
      search.setFields(list);
    }
    list.addAll(0, fields);
  }

  /**
   * Modify the search by adding the given fields before the current fields.
   */
  public static void mergeFieldsBefore(MutableSearch search, Field... fields) {
    mergeFieldsBefore(search, Arrays.asList(fields));
  }

  /**
   * Modify the search by adding the given fields after the current fields.
   */
  public static void mergeFieldsAfter(@Nonnull MutableSearch search, List<Field> fields) {
    List<Field> list = search.getFields();
    if (list == null) {
      list = new ArrayList<>();
      search.setFields(list);
    }
    list.addAll(fields);
  }

  /**
   * Modify the search by adding the given fields after the current fields.
   */
  public static void mergeFieldsAfter(MutableSearch search, Field... fields) {
    mergeFieldsAfter(search, Arrays.asList(fields));
  }

  /* Merge Filters */

  /**
   * Modify the search by adding the given filters using AND semantics.
   */
  public static void mergeFiltersAnd(@Nonnull MutableSearch search, List<Filter> filters) {
    List<Filter> list = search.getFilters();
    if (list == null) {
      list = new ArrayList<>();
      search.setFilters(list);
    }

    if (list.size() == 0 || !search.isDisjunction()) {
      search.setDisjunction(false);
      list.addAll(filters);
    } else {
      search.setFilters(new ArrayList<>());

      // add the previous filters with an OR
      Filter orFilter = Filter.or();
      orFilter.setValue(list);
      addFilter(search, orFilter);

      // add the new filters with AND
      search.setDisjunction(false);
      search.getFilters().addAll(filters);
    }
  }

  /**
   * Modify the search by adding the given filters using AND semantics
   */
  public static void mergeFiltersAnd(MutableSearch search, Filter... filters) {
    mergeFiltersAnd(search, Arrays.asList(filters));
  }

  /**
   * Modify the search by adding the given filters using OR semantics
   */
  public static void mergeFiltersOr(@Nonnull MutableSearch search, List<Filter> filters) {
    List<Filter> list = search.getFilters();
    if (list == null) {
      list = new ArrayList<>();
      search.setFilters(list);
    }

    if (list.size() == 0 || search.isDisjunction()) {
      search.setDisjunction(true);
      list.addAll(filters);
    } else {
      search.setFilters(new ArrayList<>());

      // add the previous filters with an AND
      Filter orFilter = Filter.and();
      orFilter.setValue(list);
      addFilter(search, orFilter);

      // add the new filters with or
      search.setDisjunction(true);
      search.getFilters().addAll(filters);
    }
  }

  /**
   * Modify the search by adding the given filters using OR semantics
   */
  public static void mergeFiltersOr(MutableSearch search, Filter... filters) {
    mergeFiltersOr(search, Arrays.asList(filters));
  }

  /* Other */

  /**
   * Calculation the first result value to use the {@literal firstResult}, {@literal maxResults},
   * {@literal pageNumber} values of the search object. The calculation is as follows:
   *
   * <ul>
   * <li>If {@literal firstResult} is defined (i.e. > 0), use it.
   * <li>Otherwise if {@literal pageNumber} and {@literal maxResults} are defined (i.e. > 0),
   *     use {@code pageNumber * pageSize}.
   * <li>Otherwise, just use 0.
   * </ul>
   */
  public static int calcFirstResult(@Nonnull ImmutableSearch search) {
    return (search.getFirstResult() > 0) ?
        search.getFirstResult() : (search.getPageNumber() > 0 && search.getMaxResults() > 0) ?
        search.getPageNumber() * search.getMaxResults() : 0;
  }

  /**
   * Copy the contents of the source search object to the destination search object, overriding
   * any contents previously found in the destination. All destination properties reference the
   * same objects from the source properties.
   */
  public static @Nonnull MutableSearch shallowCopy(@Nonnull ImmutableSearch source, @Nonnull MutableSearch destination) {
    destination.setSearchClass(source.getSearchClass());
    destination.setDistinct(source.isDistinct());
    destination.setDisjunction(source.isDisjunction());
    destination.setResultMode(source.getResultMode());
    destination.setFirstResult(source.getFirstResult());
    destination.setMaxResults(source.getMaxResults());
    destination.setPageNumber(source.getPageNumber());
    destination.setFetches(source.getFetches());
    destination.setFields(source.getFields());
    destination.setFilters(source.getFilters());
    destination.setSorts(source.getSorts());

    return destination;
  }

  /**
   * Copy the contents of the source search object to the destination search object, overriding
   * any contents previously found in the destination. All destination properties reference the
   * same objects from the source properties.
   */
  public static @Nonnull MutableSearch shallowCopy(ImmutableSearch source) {
    return shallowCopy(source, new Search());
  }

  /**
   * Copy the contents of the source search object to the destination search object, overriding
   * any contents previously found in the destination. All collections are copied into new
   * collections, but the items in those collections are not duplicated; they still point to
   * the same objects.
   */
  public static @Nonnull <T extends MutableSearch> T copy(ImmutableSearch source, T destination) {
    shallowCopy(source, destination);

    ArrayList<String> fetches = new ArrayList<>(source.getFetches());
    destination.setFetches(fetches);

    ArrayList<Field> fields = new ArrayList<>(source.getFields());
    destination.setFields(fields);

    ArrayList<Filter> filters = new ArrayList<>(source.getFilters());
    destination.setFilters(filters);

    ArrayList<Sort> sorts = new ArrayList<>(source.getSorts());
    destination.setSorts(sorts);

    ArrayList<String> joins = new ArrayList<>(source.getJoins());
    destination.setJoins(joins);

    return destination;
  }

  /**
   * Copy the contents of the source search object into a new search object. All collections
   * are copied into new collections, but the items in those collections are not duplicated;
   * they still point to the same objects.
   */
  public static @Nonnull MutableSearch copy(ImmutableSearch source) {
    return copy(source, new Search());
  }

  private static void appendList(StringBuilder sb, List<?> list, String separator) {
    if (list == null) {
      sb.append("null");
      return;
    }

    boolean first = true;
    for (Object o : list) {
      if (first) {
        first = false;
      } else {
        sb.append(separator);
      }
      sb.append(o);
    }
  }

  /**
   * Visit each non-null item is a list. Each item may be replaced by the visitor. The modified
   * list is returned. If removeNulls is true, any null elements will be removed from the final
   * list. If there are any modifications to be made to the list a new list is made with the
   * changes so that the original list remains unchanged. If no changes are made, the original
   * list is returned.
   */
  public static <T> List<T> walkList(List<T> list, ItemVisitor<T> visitor, boolean removeNulls) {
    if (list == null)
      return null;

    ArrayList<T> copy = null;

    int i = 0;
    for (T item : list) {
      T result = visitor.visit(item);
      if (result != item || (removeNulls && result == null)) {
        if (copy == null) {
          copy = new ArrayList<T>(list.size());
          copy.addAll(list);
        }
        copy.set(i, result);
        item = result;
      }
      i++;
    }
    if (copy != null) {
      if (removeNulls) {
        for (int j = copy.size() - 1; j >= 0; j--) {
          if (copy.get(j) == null)
            copy.remove(j);
        }
      }
      return copy;
    } else {
      return list;
    }
  }

  /**
   * Walk through a list of filters and all the sub filters, visiting each filter in the tree.
   * A {@link FilterVisitor} is used to visit each filter. {@link FilterVisitor} may replace the
   * {@link Filter} that is visiting. If it does, a new tree and list of {@link Filter}s will be
   * created for every part of the tree that is affected, thus preserving the original tree.
   *
   * @return if any changes have been made, the new list of {@link Filter}s;
   *         if not, the original list.
   */
  public static List<Filter> walkFilters(List<Filter> filters, FilterVisitor visitor, boolean removeNulls) {
    return walkList(filters, new FilterListVisitor(visitor, removeNulls), removeNulls);
  }

  /**
   * Walk a filter and all its sub filters, visiting each filter in the tree. The class
   * {@link FilterVisitor} is used to visit each filter. The {@link FilterVisitor} may replace
   * the {@link Filter} that is is visiting. If it does, a new tree and will be created for
   * every part of the tree that is affected, thus preserving the original tree.
   *
   * @return if any changes have been made, the new {@link Filter};
   *         if not, the original {@link Filter}.
   */
  @SuppressWarnings("unchecked")
  public static Filter walkFilter(Filter filter, @Nonnull FilterVisitor visitor, boolean removeNulls) {
    filter = visitor.visitBefore(filter);

    if (filter != null) {
      if (filter.isTakesSingleSubFilter()) {
        if (filter.getValue() instanceof Filter) {
          Filter result = walkFilter((Filter) filter.getValue(), visitor, removeNulls);
          if (result != filter.getValue()) {
            filter = new Filter(filter.getProperty(), result, filter.getOperator());
          }
        }
      } else if (filter.isTakesListOfSubFilters()) {
        if (filter.getValue() instanceof List) {
          List<Filter> result = walkFilters((List<Filter>) filter.getValue(), visitor, removeNulls);
          if (result != filter.getValue()) {
            filter = new Filter(filter.getProperty(), result, filter.getOperator());
          }
        }
      }
    }

    filter = visitor.visitAfter(filter);

    return filter;
  }

  /**
   * @param search current search.
   * @param join current join string.
   */
  public static void addJoin(MutableSearch search, String join) {
    if (join == null || "".equals(join))
      return; // null join do nothing, don't bother to add them.

    List<String> joins = search.getJoins();
    if (joins == null) {
      joins = new ArrayList<>();
      search.setJoins(joins);
    }
    joins.add(join);
  }

  /**
   * Return true if the search objects have equivalent contents.
   */
  public static boolean equals(ImmutableSearch search, Object obj) {
    if (search == obj)
      return true;
    if (!(obj instanceof ImmutableSearch))
      return false;

    ImmutableSearch s = (ImmutableSearch) obj;

    if (search.getSearchClass() == null ? s.getSearchClass() != null : !search.getSearchClass().equals(s.getSearchClass()))
      return false;
    if (search.isDisjunction() != s.isDisjunction() ||
        search.getResultMode() != s.getResultMode() ||
        search.getFirstResult() != s.getFirstResult() ||
        search.getMaxResults() != s.getMaxResults() ||
        search.getPageNumber() != s.getPageNumber())
      return false;

    if (search.getFetches() == null ? s.getFetches() != null : !search.getFetches().equals(s.getFetches()))
      return false;
    if (search.getFields() == null ? s.getFields() != null : !search.getFields().equals(s.getFields()))
      return false;
    if (search.getFilters() == null ? s.getFilters() != null : !search.getFilters().equals(s.getFilters()))
      return false;
    return search.getSorts() == null ? s.getSorts() == null : search.getSorts().equals(s.getSorts());
  }

  /**
   * Return a hash code value for the given search.
   */
  public static int hashCode(@Nonnull ImmutableSearch search) {
    int hash = 1;
    hash = hash * 31 + (search.getSearchClass() == null ? 0 : search.getSearchClass().hashCode());
    hash = hash * 31 + (search.getFields() == null ? 0 : search.getFields().hashCode());
    hash = hash * 31 + (search.getFilters() == null ? 0 : search.getFilters().hashCode());
    hash = hash * 31 + (search.getSorts() == null ? 0 : search.getSorts().hashCode());
    hash = hash * 31 + (search.isDisjunction() ? 1 : 0);
    hash = hash * 31 + search.getResultMode().hashCode();
    hash = hash * 31 + (Integer.valueOf(search.getFirstResult()).hashCode());
    hash = hash * 31 + (Integer.valueOf(search.getMaxResults()).hashCode());
    hash = hash * 31 + (Integer.valueOf(search.getPageNumber()).hashCode());

    return hash;
  }

  /**
   * Return a human-readable string describing the contents of the given search.
   */
  public static @Nonnull String toString(@Nonnull ImmutableSearch search) {
    StringBuilder sb = new StringBuilder("Search(");
    sb.append(search.getSearchClass());
    if (search.getJoins() != null && search.getJoins().size() > 0) {
      sb.append("), customs: (").append(search.getJoins());
    }
    sb.append(")[first result: ").append(search.getFirstResult());
    sb.append(", max results: ").append(search.getMaxResults());
    sb.append(", page number: ").append(search.getPageNumber());
    sb.append("] {\n resultMode: ");

    switch (search.getResultMode()) {
      case RESULT_AUTO:
        sb.append("AUTO");
        break;
      case RESULT_ARRAY:
        sb.append("ARRAY");
        break;
      case RESULT_LIST:
        sb.append("LIST");
        break;
      case RESULT_MAP:
        sb.append("MAP");
        break;
      case RESULT_SINGLE:
        sb.append("SINGLE");
        break;
      default:
        sb.append("**INVALID RESULT MODE: (").append(search.getResultMode()).append(")**");
        break;
    }

    sb.append(",\n disjunction: ").append(search.isDisjunction());
    sb.append(",\n fields: { ");
    appendList(sb, search.getFields(), ", ");
    sb.append(" },\n filters: {\n  ");
    appendList(sb, search.getFilters(), ",\n  ");
    sb.append("\n },\n sorts: { ");
    appendList(sb, search.getSorts(), ", ");
    sb.append(" }\n}");

    return sb.toString();
  }

  /**
   * Visitor for use with walkList()
   */
  public static class ItemVisitor<T> {

    public T visit(T item) {
      return item;
    }

  }

  /**
   * Visitor for use with walkFilter and walkFilters.
   */
  public static class FilterVisitor {

    public Filter visitBefore(Filter filter) {
      return filter;
    }

    public Filter visitAfter(Filter filter) {
      return filter;
    }

  }

  /**
   * Used in walkFilters.
   */
  private static final class FilterListVisitor extends ItemVisitor<Filter> {

    private final FilterVisitor visitor;
    private final boolean removeNulls;

    public FilterListVisitor(FilterVisitor visitor, boolean removeNulls) {
      this.visitor = visitor;
      this.removeNulls = removeNulls;
    }

    @Override
    public Filter visit(Filter filter) {
      return walkFilter(filter, visitor, removeNulls);
    }

  }

}
