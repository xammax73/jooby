package io.xammax.jooby.internal.hibernate;

import java.util.List;

public interface ImmutableSearch {

  /**
   * Gets zero based index of first result record to return.
   *
   * @return number of first record
   */
  int getFirstResult();

  /**
   * Sets the maximum number of records to return. Also used as page size when calculating the
   * first record to return based {@literal maxResults} on page. If value {@code maxResults=0},
   * the value is unspecified.
   *
   * @return maximum number of records on page
   */
  int getMaxResults();

  /**
   * Zero based number of the page of records to return ({@literal pageNumber}). The size of
   * page is determined by {@literal maxResults}. If both parameters {@literal pageNumber} and
   * {@literal maxResults} are specified (i.e. > 0), the {@literal firstResult} returned is
   * calculated as {@code pageNumber * maxResults}. The {@literal firstResult} has precedence
   * over {@literal pageNumber}. If {@literal firstResult} is specified (i.e. > 0),
   * {@literal pageNumber} is ignored.
   */
  int getPageNumber();

  /**
   * Gets search query class.
   *
   * @return class {@link Search}
   */
  Class<?> getSearchClass();

  /**
   * Gets list of {@link String} fetches.
   *
   * @return list of {@link String} fetches
   */
  List<String> getFetches();

  /**
   * Gets list of {@link Field} fields.
   *
   * @return list of {@link Field} fields
   */
  List<Field> getFields();

  /**
   * Gets list of {@link Filter} filters.
   *
   * @return list of {@link Filter} filters
   */
  List<Filter> getFilters();

  /**
   * Gets list of {@link Sort} sorts.
   *
   * @return list of {@link Sort} sorts
   */
  List<Sort> getSorts();

  /**
   * Gets boolean value about uses disjunction.
   *
   * @return {@literal true} if uses disjunction, otherwise {@literal false}
   */
  boolean isDisjunction();

  /**
   * Gets boolean value about uses distinct.
   *
   * @return {@literal true} if uses distinct, otherwise {@literal false}
   */
  boolean isDistinct();

  /**
   * Gets list of {@link String} of all joins.
   *
   * @return joins (inner or outer)
   */
  List<String> getJoins();

  /**
   * Gets result mode tells the {@link Search} what form to use for the results. Options
   * includes all values of {@link Result}.
   *
   * @return mode of {@link Result}
   */
  Result getResultMode();

  /**
   * Gets result map type class.
   *
   * @return result map type class. Parameter {@link ImmutableSearch#getResultMode()} is ignored
   * if resultMapClass is set.
   */
  Class<?> getResultMapClass();

}
