package io.xammax.jooby.internal.hibernate;

public enum Result {

  /**
   * Value for result mode. This is default value. With {@code RESULT_AUTO} the result mode is
   * automatically determined according to the following rules: if any field is specified with
   * a key, use {@code RESULT_MAP}, if value equals zero or one fields are specified, use
   * {@code RESULT_SINGLE}, otherwise, use {@code RESULT_ARRAY}.
   *
   * @see ImmutableSearch#getResultMode()
   */
  RESULT_AUTO,

  /**
   * Value for result mode. {@code RESULT_ARRAY} returns each result as an Object array
   * ({@code Object[]}) with the entries corresponding to the fields added to the search.
   * For example:
   *
   * {@code
   *     Search s = new Search(Person.class);
   *     s.setResultMode(Search.RESULT_ARRAY);
   *     s.addField("firstName");
   *     s.addField("lastName");
   *     for (Object[] array : repository.search(s)) {
   *         System.out.println(array[0] + " " + array[1]);
   *     }
   * }
   *
   * @see ImmutableSearch#getResultMode()
   */
  RESULT_ARRAY,

  /**
   * Value for result mode. {@code RESULT_LIST} returns each result as {@code List<Object>}.
   * For example:
   *
   * {@code
   *     Search s = new Search(Person.class);
   *     s.setResultMode(Search.RESULT_LIST);
   *     s.addField("firstName");
   *     s.addField("lastName");
   *     for (List<Object> list : repository.search(s)) {
   *         System.out.println(list.get(0) + " " + list.get(1));
   *     }
   * }
   *
   * @see ImmutableSearch#getResultMode()
   */
  RESULT_LIST,

  /**
   * Value for result mode. {@code RESULT_MAP} returns each row as a map with properties names
   * or keys for keys to the corresponding values. For example:
   *
   * {@code
   *     Search s = new Search(Person.class);
   *     s.setResultMode(Search.RESULT_MAP;
   *     s.addField("firstName");
   *     s.addField("lastName", "ln");
   *     for (Map<String, Object> map : repository.search(s)) {
   *         System.out.println(map.get("firstName") + " " + map.get("ln"));
   *     }
   * }
   *
   * @see ImmutableSearch#getResultMode()
   */
  RESULT_MAP,

  /**
   * Value for result mode. {@code RESULT_SINGLE} - exactly one field or no fields must be
   * specified to use this result mode. The result list contains just the value of that property
   * for each element. For example:
   *
   * {@code
   *     Search s = new Search(Person.class);
   *     s.setResultMode(Search.RESULT_SINGLE);
   *     s.addField("firstName");
   *     for (Object name : dao.search(s)) {
   *         System.out.println(name);
   *     }
   * }
   *
   * @see ImmutableSearch#getResultMode()
   */
  RESULT_SINGLE

}
