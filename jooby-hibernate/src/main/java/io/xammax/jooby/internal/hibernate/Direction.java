package io.xammax.jooby.internal.hibernate;

import java.util.Locale;
import java.util.Optional;

/**
 * Enumeration for sort directions.
 */
public enum Direction {

  ASC, DESC;

  /**
   * Returns whether the direction is ascending.
   *
   * @return {@literal true} if {@link Direction} is ASC, otherwise {@literal false}
   */
  public boolean isAscending() {
    return this.equals(ASC);
  }

  /**
   * Returns whether the direction is descending.
   *
   * @return {@literal true} if {@link Direction} is DESC, otherwise {@literal false}
   */
  public boolean isDescending() {
    return this.equals(DESC);
  }

  /**
   * Returns the {@link Direction} enum for the given {@literal value}.
   *
   * @param value ASC or DESC
   * @return ASC or DESC to upperCase
   * @throws IllegalArgumentException in case the value cannot be parsed into an enum value
   */
  public static Direction fromString(String value) {
    try {
      return Direction.valueOf(value.toUpperCase(Locale.US));
    } catch (Exception e) {
      throw new IllegalArgumentException(String.format(
          "Invalid value '%s' for sorts given! Has to be either 'desc' or 'asc' (case insensitive).", value), e);
    }
  }

  /**
   * Returns the {@link Direction} for the given {@literal value} or {@literal null} if it cannot
   * be parsed into an enum value.
   *
   * @param value ASC or DESC
   * @return ASC or DESC to upperCase
   */
  public static Optional<Direction> fromOptionalString(String value) {
    try {
      return Optional.of(fromString(value));
    } catch (IllegalArgumentException e) {
      return Optional.empty();
    }
  }

}