package io.xammax.jooby.internal.hibernate;

import java.util.Locale;

/**
 * Enumeration for filter operator.
 */
public enum FilterOperator {

  OP_EQUAL,
  OP_NOT_EQUAL,
  OP_LESS_THAN,
  OP_GREATER_THAN,
  OP_LESS_OR_EQUAL,
  OP_GREATER_OR_EQUAL,
  OP_IN,
  OP_NOT_IN,
  OP_LIKE,
  OP_ILIKE,
  OP_NULL,
  OP_NOT_NULL,
  OP_EMPTY,
  OP_NOT_EMPTY,
  OP_AND,
  OP_OR,
  OP_NOT,
  OP_SOME,
  OP_NONE,
  OP_ALL,
  OP_CUSTOM;

  /**
   * Returns the {@link FilterOperator} enum for the given {@link String} value.
   *
   * @param value from {@link FilterOperator} enum, not require uppercase
   * @return {@link FilterOperator} enum to upperCase
   * @throws IllegalArgumentException if the given value cannot be parsed into an enum value
   */
  public static FilterOperator fromString(String value) {
    try {
      return FilterOperator.valueOf(value.toUpperCase(Locale.US));
    } catch (Exception e) {
      throw new IllegalArgumentException(String.format("Invalid value '%s' for filters given!", value), e);
    }
  }

}
